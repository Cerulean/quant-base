FROM ceruleanwang/ubuntu-base

# RUN /bin/bash -c 'pip3 install --upgrade pip3==9.0.1'

# rqalpha
RUN /bin/bash -c 'pip3 install rqalpha'

# tushare
RUN /bin/bash -c 'pip3 install lxml'
RUN /bin/bash -c 'pip3 install requests'
RUN /bin/bash -c 'pip3 install bs4'
RUN /bin/bash -c 'pip3 install tushare'


# others

RUN /bin/bash -c 'pip3 install scipy'
RUN /bin/bash -c 'pip3 install tornado'
RUN /bin/bash -c 'pip3 install mongoengine'
RUN /bin/bash -c 'pip3 install scrapy'
RUN /bin/bash -c 'pip3 install sklearn'

